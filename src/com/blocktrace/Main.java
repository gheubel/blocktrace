package com.blocktrace;

import com.blocktrace.commands.Rollback;
import com.blocktrace.commands.TraceLog;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main extends JavaPlugin {
    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("HH:mm:ss, MM/dd/yy");
    private static Main instance;

    @Override
    public void onEnable() {
        instance = this;
        List<String> formattedLog = new ArrayList<String>() {};
        Trace c = new Trace(formattedLog);

        System.out.println("Blocktrace has been enabled");

        // Events
        Bukkit.getPluginManager().registerEvents(c, this);

        // Commands
        new TraceLog(c);
        new Rollback();
    }

    @Override
    public void onDisable() {
        System.out.println("Blocktrace has been disabled");
    }

    public static Main getInstance() {
        return instance;
    }
}