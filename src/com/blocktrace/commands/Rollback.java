package com.blocktrace.commands;

import com.blocktrace.util.CommandBase;
import com.blocktrace.util.Message;
import com.blocktrace.util.ReverseLineInputStream;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.blocktrace.Main.DATE_FORMAT;

public class Rollback {
    public Rollback() {
        new CommandBase("rollback", false)  {
            @Override
            public boolean onCommand(CommandSender sender, String[] arguments) {
                List<String> lines = new ArrayList<>() {};
                List<String> cmds = new ArrayList<>() {};

                try {
                    File file = new File("./.tracelog");
                    BufferedReader in = new BufferedReader(new InputStreamReader(new ReverseLineInputStream(file)));
                    String l = in.readLine();
                    while (l != null && !(l.contains("ROLLBACK"))) {
                        lines.add(l);
                        l = in.readLine();
                    }
                    in.close();
                } catch (IOException | NullPointerException e) { e.printStackTrace(); }

                char op1 = '>';
                char op2 = '<';

                for (String s : lines) {
                    String new_str = "setblock ";
                    for (char c : s.toCharArray()) {
                        if (c == op1) {
                            new_str += "air";
                            break;
                        }

                        else if (c == op2) {
                            continue;
                        }

                        else if (c == '|') {
                            break;
                        }

                        else {
                            new_str += c;
                        }
                    }
                    cmds.add(new_str);
                }

                for (String s : cmds) {
                    System.out.println("Executing \'" + s + "\'");
                    Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), s);
                }

                try {
                    FileWriter f = new FileWriter(".tracelog", true);
                    f.append("ROLLBACK | " + sender.getName() + " " + System.currentTimeMillis() + "\n");
                    f.close();
                } catch (IOException e) { e.printStackTrace(); }

                if (sender instanceof Player) {
                    Message.send((Player)sender, "&6Successfully rolled back &f" + cmds.size() + " &6events at &7"
                            + DATE_FORMAT.format(Calendar.getInstance().getTime()));
                }

                return true;
            }

            @Override
            public String getUsage() {
                return "/rollback";
            }

            @Override
            public String getDescription() {
                return "Reverses previous place and break events";
            }
        };
    }
}