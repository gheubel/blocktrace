package com.blocktrace.commands;

import com.blocktrace.util.CommandBase;
import com.blocktrace.Trace;
import com.blocktrace.util.Message;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TraceLog {
    public Trace c;
    public TraceLog(Trace inC) {
        this.c = inC;
        new CommandBase("tracelog", true) {
            @Override
            public boolean onCommand (CommandSender sender, String[] arguments) {
                int len = c.formattedLog.size();
                Player player = (Player)sender;

                if (len >= 10) {
                    Message.send(player, "&a--- Past 10 updates in .tracelog ---");
                    for (int i = (len - 10); i < len; i++) {
                        Message.send(player, c.formattedLog.get(i));
                    }
                }

                else {
                    Message.send(player, "&a--- Past " + len + " updates in .tracelog ---");
                    for (int i = 0; i < len; i++) {
                        Message.send(player, c.formattedLog.get(i));
                    }
                }

                Message.send(player, "&a-------------------------------");
                return true;
            }

            @Override
            public String getUsage() {
                return "/tracelog";
            }

            @Override
            public String getDescription() {
                return "Returns the most recent .tracelog updates";
            }
        };
    }
}
