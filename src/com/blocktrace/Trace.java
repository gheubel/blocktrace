package com.blocktrace;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.EntityBlockFormEvent;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import static com.blocktrace.Main.DATE_FORMAT;

public class Trace implements Listener {
    public List<String> formattedLog;

    public Trace(List<String> formattedLog) {
        this.formattedLog = formattedLog;
    }

    public void appendToChatLog(String s) {
        this.formattedLog.add(s);
    }

    @EventHandler
    public boolean onBlockPlace(BlockPlaceEvent event) throws IOException {
        String[] coords =
                { Integer.toString(event.getBlock().getX()) ,
                  Integer.toString(event.getBlock().getY()) ,
                  Integer.toString(event.getBlock().getZ()) };

        String time = DATE_FORMAT.format(Calendar.getInstance().getTime());
        String name = event.getPlayer().getName();
        String type = event.getBlock().getBlockData().getAsString();
        String coordsStr = Arrays.toString(coords);

        String logOutput = coords[0] + " " + coords[1] + " " + coords[2] + " >" + type + " | " + System.currentTimeMillis() + " " + name + "\n";
        FileWriter file = new FileWriter(".tracelog", true);
        file.append(logOutput);
        file.close();

        String chatType = "";

        for (char c : type.toCharArray()) {
            if (c != '[') {
                chatType += c;
            }

            else {
                break;
            }
        }

        String chatOutput = "&8(" + time + "): &7" + name + " &2placed &7" + chatType + " &8at &7" + coordsStr + "\n";
        appendToChatLog(chatOutput);

        return true;
    }

    @EventHandler
    public boolean onBlockBreak(BlockBreakEvent event) throws IOException {
        String[] coords =
                { Integer.toString(event.getBlock().getX()) ,
                  Integer.toString(event.getBlock().getY()) ,
                  Integer.toString(event.getBlock().getZ()) };

        String time = DATE_FORMAT.format(Calendar.getInstance().getTime());
        String name = event.getPlayer().getName();
        String type = event.getBlock().getBlockData().getAsString();
        String coordsStr = Arrays.toString(coords);

        String logOutput = coords[0] + " " + coords[1] + " " + coords[2] + " <" + type + " | " + System.currentTimeMillis() + " " + name + "\n";
        FileWriter file = new FileWriter(".tracelog", true);
        file.append(logOutput);
        file.close();

        String chatType = "";

        for (char c : type.toCharArray()) {
            if (c != '[') {
                chatType += c;
            }

            else {
                break;
            }
        }

        String chatOutput = "&8(" + time + "): &7" + name + " &cbroke &7" + chatType + " &8at &7" + coordsStr + "\n";
        appendToChatLog(chatOutput);

        return true;
    }
}
