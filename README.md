# blocktrace

1.16.5 Minecraft Spigot server plugin that logs players' placing and breaking of blocks. [JDK 16 required](https://jdk.java.net/archive/).

![](https://media.discordapp.net/attachments/793267960252727368/930710837440708618/the.gif)
